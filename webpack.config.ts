import path from 'path'
import webpack from 'webpack'
import moment from 'moment'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import TerserJSPlugin from 'terser-webpack-plugin'
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin'
import WorkboxPlugin from 'workbox-webpack-plugin'
import { Configuration } from './types/webpack'

import description from './package.json'

function webpackConfig (env: any, argv: any): Configuration {
  const mode = argv.mode || 'development'
  const devMode = mode === 'development'

  return {
    entry: {
      bundle: './front/index.tsx',
    },
    output: {
      path: path.resolve(__dirname, 'public'),
      filename: devMode
        ? '[name].js'
        : '[name].[contenthash].js',
      publicPath: '/lego',
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          use: { loader: 'ts-loader' },
        },
        {
          test: /\.css$/,
          use: [
            devMode
              ? 'style-loader'
              : MiniCssExtractPlugin.loader,
            'css-loader',
          ],
        },
        {
          test: /\.scss$/,
          use: [
            devMode
              ? 'style-loader'
              : MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
          ],
        },

        {
          test: /\.(png|jpg|svg|ttf|eot|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          use: {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        },
      ],
    },
    resolve: {
      extensions: [
        '.ts',
        '.tsx',
        '.js',
      ],
      mainFields: ['module', 'browser', 'main'],
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.VERSION': JSON.stringify(description.version),
      }),
      new HtmlWebpackPlugin({
        title: 'LEGO Duplo',
        template: 'front/index.ejs',
        filename: 'index.html',
        version: description.version,
        build: process.env.CI && process.env.CI_COMMIT_REF_NAME !== 'master_remove'
          ? ` от ${moment().utcOffset('+03:00').format('DD.MM.YYYY HH:mm')} (${process.env.CI_COMMIT_SHORT_SHA})`
          : '',
        NODE_ENV: mode,
      }),
      new webpack.HashedModuleIdsPlugin(),
      new MiniCssExtractPlugin({
        filename: devMode
          ? 'style.css'
          : '[name].[id].[contenthash].css',
      }),
      new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
      new WorkboxPlugin.GenerateSW({
        swDest: 'sw.js',
        clientsClaim: true,
        skipWaiting: true,
      }),
    ],
    devtool: devMode
      ? 'inline-cheap-module-source-map'
      : false,
    devServer: {
      https: true,
      port: 443,
      open: true,
      contentBase: './public',
      historyApiFallback: true,
    },
    optimization: {
      minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    },
  }
}

module.exports = webpackConfig
