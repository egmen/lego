import Promise from "bluebird";
import React from "react";
import ReactDOM from "react-dom";
import { observer } from "mobx-react";
import "moment/locale/ru";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/free-solid-svg-icons";

import duplo from "./duplo";

@observer
export class DuploTrain extends React.Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <>
        <p>
          Color: {duplo.colorName} ({duplo.colorId})
        </p>
        <p>Speed: {duplo.train?.speed}</p>
        <p>Current power: {duplo.power}</p>
        <input
          type="range"
          min={-100}
          max={100}
          style={{ width: "80%" }}
          value={duplo.power}
          onChange={duplo.changePower}
        />
        <hr />
        <Button variant="outline-secondary" onClick={duplo.setRandomColor}>
          setRandomColor
        </Button>
        <hr />
        <Button variant="outline-secondary" onClick={duplo.stop}>
          stops
        </Button>
        <hr />
        <Button variant="outline-secondary" onClick={() => duplo.playSound()}>
          <FontAwesomeIcon icon={faCircle} />
        </Button>
        {duplo.colorIcons.map((item) => (
          <Button
            key={item.id}
            variant="outline-secondary"
            onClick={() => duplo.playSound(item.id)}
          >
            <FontAwesomeIcon icon={item.icon} />
          </Button>
        ))}
        <hr />
        <textarea
          style={{ width: "80%" }}
          readOnly
          rows={20}
          value={duplo.textLog}
        />
      </>
    );
  }
}
