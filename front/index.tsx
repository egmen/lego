import Promise from "bluebird";
import React from "react";
import ReactDOM from "react-dom";
import { observer } from "mobx-react";
import "moment/locale/ru";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faTrain } from "@fortawesome/free-solid-svg-icons";
import "bootstrap/dist/css/bootstrap.min.css";

import duplo from "./duplo";
import { DuploTrain } from "./DuploTrain";

@observer
class App extends React.Component {
  constructor(props: any) {
    super(props);
  }
  render() {
    return (
      <>
        <Navbar bg="dark" variant="dark" onSelect={duplo.handleNavbarSelect}>
          <Container>
            <Navbar.Brand>Train</Navbar.Brand>
            <Nav>
              {duplo.trains.map((item) => (
                <Nav.Link
                  key={item.id}
                  eventKey={item.id}
                  active={duplo.activeTrain === item.id}
                >
                  <FontAwesomeIcon icon={faTrain} />
                  &nbsp;
                  {item.name}
                </Nav.Link>
              ))}
              <Nav.Link eventKey="plus" title="Добавить">
                <FontAwesomeIcon icon={faPlus} />
              </Nav.Link>
            </Nav>
          </Container>
        </Navbar>
        <Container fluid={true}>
          <Row>
            <Col>
              <DuploTrain />
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("main"));
