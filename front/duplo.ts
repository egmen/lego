import _ from "lodash";
import moment from "moment";
import { observable, action, computed, reaction, autorun } from "mobx";
import "node-poweredup/dist/browser/poweredup";
import { DuploTrainBase, Consts, isWebBluetooth } from "node-poweredup";
import {
  faSignal,
  faStop,
  faTrain,
  faWater,
  faWind,
  IconDefinition,
} from "@fortawesome/free-solid-svg-icons";
import { DuploTrainBaseSound } from "node-poweredup/dist/node/consts";
// @ts-ignore
const { PoweredUP } = window.PoweredUP;

export type DuploTrain = {
  id: string;
  name: string;
  speed: number;
};

export type DuploSoundIcon = {
  id: DuploTrainBaseSound;
  icon: IconDefinition;
};

export class DuploStore {
  @observable public colorId = 0;
  @observable public ledColor = 0;
  @observable public power = 0;
  @observable public arrayLog: string[] = [];

  @observable public activeTrain: string | null = null;

  @observable public trains: DuploTrain[] = [];

  poweredUP = new PoweredUP();
  hub: DuploTrainBase | null = null;

  colorIcons: DuploSoundIcon[] = [
    {
      id: DuploTrainBaseSound.BRAKE,
      icon: faStop,
    },
    {
      id: DuploTrainBaseSound.HORN,
      icon: faSignal,
    },
    {
      id: DuploTrainBaseSound.STATION_DEPARTURE,
      icon: faTrain,
    },
    {
      id: DuploTrainBaseSound.STEAM,
      icon: faWater,
    },
    {
      id: DuploTrainBaseSound.WATER_REFILL,
      icon: faWind,
    },
  ];

  constructor() {
    reaction(
      () => this.power,
      async () => {
        await this.hub?.setMotorSpeed("MOTOR", this.power);
      }
    );
    reaction(
      () => this.ledColor,
      () => this.hub?.setLEDColor(this.ledColor)
    );
    console.log(this.poweredUP);
    this.poweredUP.on("discover", async (hub: DuploTrainBase) => {
      this.log(`Connecting... ${hub.name}!`);
      console.log("Connecting", hub);

      await hub.connect();
      await hub.sleep(1000);
      this.hub = hub;
      this.ensureHub(hub);
      if (!this.activeTrain) {
        this.activeTrain = hub.primaryMACAddress;
      }
      this.log(`Connected 2 to ${hub.name} (${hub.primaryMACAddress})!`);

      hub.on("disconnect", () => {
        this.log(`Disconnected ${hub.name}`);
      });
      hub.on("color", (port: string, color: number) => {
        this.colorId = color;
        this.log(`Color detected on port ${port} (Color: ${color})`);
      });
      hub.on("attach", (event: any) => {
        console.log(event);
        this.log(
          `Device attached to port ${event.typeName} (${event.portName}) (Device ID: ${event.portId})`
        );
      });
      hub.on("detach", (port: string) => {
        this.log(`Device detached from port ${port}`);
      });

      hub.on("speed", (id: string, speed: number) => {
        console.log("received speed", this.activeTrain || "");
        this.setSpeed(this.activeTrain || "", speed);
      });
    });
  }

  @action ensureHub(hub: DuploTrainBase): void {
    const mac = hub.primaryMACAddress;
    if (!this.trains.find(({ id }) => id === mac)) {
      const newTrain: DuploTrain = {
        id: mac,
        name: mac,
        speed: 0,
      };
      this.trains.push(newTrain);
    }
  }

  @action public scan = () => {
    this.log("Scanning...");
    if (isWebBluetooth) {
      this.poweredUP.scan();
    } else {
      alert("Your browser does not support the Web Bluetooth specification.");
    }
  };

  @action private setSpeed(id: string, speed: number): void {
    const currentTrain = this.trains.find((train) => train.id === id);
    if (currentTrain) {
      currentTrain.speed = speed;
    }
  }
  @action public log(text: string) {
    console.log(text);
    this.arrayLog.push(`${moment().format("HH:mm:ss.S")}: ${text}`);
  }
  @action public stop = () => {
    this.power = 0;
    this.log("Stop");
  };
  @action public setRandomColor = () => {
    const color = _.random(1, 10);
    this.ledColor = color;
    this.log(`setRandomColor ${Consts.Color[color]} (${color})`);
  };
  @action public changePower = (event: any) => {
    const power = Number(event.target.value);
    this.power = power;
  };
  @action public playSound = async (id?: number) => {
    const randomSound = _.random(0, 4);
    const sounds = _.chain(Consts.DuploTrainBaseSound)
      .map()
      .filter(_.isNumber)
      .value();
    const soundId = id ?? sounds[randomSound];
    await this.hub?.playSound(soundId);
    this.log(`playSound ${Consts.DuploTrainBaseSound[soundId]} (${soundId})`);
  };

  @action public handleNavbarSelect = (eventKey: string | null) => {
    console.log({ eventKey });
    if (eventKey === "plus") {
      return this.scan();
    }
  };

  @computed get colorName() {
    return Consts.Color[this.colorId];
  }
  @computed get textLog() {
    const textLog = this.arrayLog.slice(-20).reverse().join("\n");
    return textLog;
  }

  @computed get train(): DuploTrain | null {
    const item = this.trains.find(({ id }) => id === this.activeTrain);
    if (item) {
      return item;
    }
    return null;
  }
}

export default new DuploStore();
